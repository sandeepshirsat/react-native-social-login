// https://www.youtube.com/watch?v=6stMRtgQopk&list=LLI-Rt5LajvzpiOtZR_jXIyA&index=2&t=70s
// profile url : https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=599744900858457&height=10&width=10&ext=1582310649&hash=AeSHk1mjgSZ0hAyz

// https://www.djamware.com/post/5d9d4aeec1fff332c94be1ea/react-native-tutorial-facebook-login-example

import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, Alert, Image } from 'react-native';
import axios from 'axios'

import * as Facebook from 'expo-facebook';

export default function FacebookLogin() {

  const [Visible, setVisible] = useState(false)

  const [data, setdata] = useState({
    'id': '',
    'image': '',
    'fullname': '',
    'email': ''
  })

  async function loginWithFB() {
    try {
      await Facebook.initializeAsync('663500794457425');
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email'],
      });
      if (type === 'success') {

        console.log("Token =" + token);

        const handleFeedbackReponse = async () => {
          axios({
            method: 'get',
            url: "https://graph.facebook.com/me?access_token=" + token + "&fields=id,name,email,about,picture"
          })
            .then(function (response) {

              console.log('\n===Response ===\n' + JSON.stringify(response.data))

              setVisible(true)

              setdata({
                ...data,
                ['id']: response.data.id,
                ['image']: "https://graph.facebook.com/" + response.data.id + "/picture?type=large",
                ['fullname']: response.data.name,
                ['email']: response.data.email
              })

            })
            .catch(function (error) {
              console.log(error);

            });
        }; handleFeedbackReponse();


      } else {



      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  const logout = () => {

    setVisible(false)

    setdata({ ...data, ['id']: "", ["image"]: "", ['fullname']: "", ['email']: "" })

  }

  return (
    <View style={styles.container}>

      {

        Visible === true ?

          <View style={styles.sub_container}>

            <Image
              style={{ marginTop: 10, width: 120, height: 120, borderRadius: 120 / 2 }}
              source={{ uri: data.image }}
            />

            <View style={{ flexDirection: 'row', marginTop: 10 }}>

              <Text style={styles.boldText}>
                Facebook ID :
              </Text>

              <Text style={styles.simpleText}>
                {data.id}
              </Text>

            </View>

            <View style={{ flexDirection: 'row', marginTop: 10 }}>

              <Text style={styles.boldText}>
                Name :
              </Text>

              <Text style={styles.simpleText}>
                {data.fullname}
              </Text>

            </View>

            <View style={{ flexDirection: 'row', marginTop: 10 }}>

              <Text style={styles.boldText}>
                Email ID :
              </Text>

              <Text style={styles.simpleText}>
                {data.email}
              </Text>

            </View>

          </View>
          :

          null
      }

      {

        data.id != "" ?

          <TouchableOpacity onPress={() => logout()}>
            <Text style={{ color: 'white', fontWeight: 'bold', backgroundColor: 'blue', borderRadius: 10, padding: 10, marginTop: 20 }}>
              Logout
            </Text>
          </TouchableOpacity>

          :

          <TouchableOpacity onPress={() => loginWithFB()}>

            <View style={{ width: 180, flexDirection: 'row', borderWidth: 2, borderColor: 'lightgrey' }}>
              <Image source={require('./assets/facebook.png')} style={{
                height: 30, width: 30, alignContent: 'center', alignItems: 'center', alignSelf: 'center', marginLeft: 5
              }} />
              <Text style={{ color: 'black', fontWeight: 'bold', backgroundColor: 'white', borderColor: 'black', borderWidth: 0, borderRadius: 10, padding: 10 }}>
                Login to Facebook
              </Text>
            </View>

          </TouchableOpacity>

      }

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  sub_container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  boldText: {
    color: 'black',
    fontWeight: 'bold'
  },

  simpleText: {
    color: 'black',
    fontWeight: '300'
  }

});
