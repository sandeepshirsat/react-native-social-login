import React, { useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, Alert, Image } from 'react-native';

import * as Google from 'expo-google-app-auth';

export default function GoogleLogin() {





  // For Install openssl 
  // https://stackoverflow.com/questions/11896304/openssl-is-not-recognized-as-an-internal-or-external-command

  // Generate SHA-1 Key For Android 
  // https://aboutreact.com/getting-sha1-fingerprint-for-google-api-console/

  // https://www.youtube.com/watch?v=I8GIcqp7sSY

  // https://docs.expo.io/versions/latest/sdk/google/

  // Android Client ID : 253745528916-k58279ma9nfq911mv2ls7rrvun1ftpnp.apps.googleusercontent.com
  // IOS Client ID : 253745528916-ajvaj3mt307817jltlogr159dfdv5kdi.apps.googleusercontent.com

  const [Visible, setVisible] = useState(false)

  const [data, setdata] = useState({
    "accessToken": "",
    'id': '',
    'image': '',
    'fullname': '',
    'email': ''
  })

  async function signInWithGoogleAsync() {

    try {
      const result = await Google.logInAsync({
        androidClientId: "253745528916-k58279ma9nfq911mv2ls7rrvun1ftpnp.apps.googleusercontent.com",
        iosClientId: "253745528916-ajvaj3mt307817jltlogr159dfdv5kdi.apps.googleusercontent.com",
        scopes: ['profile', 'email'],
      });

      if (result.type === 'success') {

        console.log("Details :" + JSON.stringify(result))

        setVisible(true)

        setdata({
          ...data,
          ['accessToken']: result.accessToken,
          ['id']: result.user.id,
          ['image']: result.user.photoUrl,
          ['fullname']: result.user.name,
          ['email']: result.user.email
        })

        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  }

  async function logout() {

    setVisible(false)

    setdata({
      ...data,
      ['accessToken']: "",
      ['id']: "",
      ['image']: "",
      ['fullname']: "",
      ['email']: ""
    })

  }


  return (

    <View style={styles.container}>

      {

        Visible === true ?

          <View style={styles.sub_container}>

            <Image
              style={{ marginTop: 10, width: 120, height: 120, borderRadius: 120 / 2 }}
              source={{ uri: data.image }}
            />

            <View style={{ flexDirection: 'column', marginTop: 10, marginLeft: 50, marginRight: 50 }}>

              <Text style={styles.boldText}>
                accessToken :
        </Text>

              <Text style={{ color: 'black', fontWeight: '300' }}>
                {data.accessToken}
              </Text>

            </View>

            <View style={{ flexDirection: 'row', marginTop: 10 }}>

              <Text style={styles.boldText}>
                Google ID :
        </Text>

              <Text style={styles.simpleText}>
                {data.id}
              </Text>

            </View>

            <View style={{ flexDirection: 'row', marginTop: 10 }}>

              <Text style={styles.boldText}>
                Name :
        </Text>

              <Text style={styles.simpleText}>
                {data.fullname}
              </Text>

            </View>

            <View style={{ flexDirection: 'row', marginTop: 10 }}>

              <Text style={styles.boldText}>
                Email ID :
        </Text>

              <Text style={styles.simpleText}>
                {data.email}
              </Text>

            </View>

          </View>

          :

          null

      }

      {

        data.accessToken != "" ?

          <TouchableOpacity onPress={() => logout()}>
            <Text style={{ color: 'white', fontWeight: 'bold', backgroundColor: 'blue', borderRadius: 10, padding: 10, marginTop: 20 }}>
              Logout
            </Text>
          </TouchableOpacity>

          :

          <TouchableOpacity onPress={() => signInWithGoogleAsync()}>

            <View style={{ width: 180, flexDirection: 'row', borderWidth: 2, borderColor: 'lightgrey', }}>
              <Image source={require('./assets/gmail.png')} style={{
                height: 30, width: 30, alignContent: 'center', alignItems: 'center', alignSelf: 'center', marginLeft: 5
              }} />
              <Text style={{ color: 'black', fontWeight: 'bold', backgroundColor: 'white', borderColor: 'black', borderWidth: 0, borderRadius: 10, padding: 10 }}>
                Login to Gmail
              </Text>
            </View>

          </TouchableOpacity>

      }

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  sub_container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  boldText: {
    color: 'black',
    fontWeight: 'bold'
  },

  simpleText: {
    color: 'black',
    fontWeight: '300'
  }

});
