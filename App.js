// https://www.youtube.com/watch?v=6stMRtgQopk&list=LLI-Rt5LajvzpiOtZR_jXIyA&index=2&t=70s
// profile url : https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=599744900858457&height=10&width=10&ext=1582310649&hash=AeSHk1mjgSZ0hAyz

import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, Alert, ScrollView } from 'react-native';
import FacebookLogin from './FacebookLogin'
import GoogleLogin from './GoogleLogin'

export default function App() {

  return (
    <View style={styles.container}>

      <FacebookLogin />

      <View style={{ marginTop: 20 }} ></View>

      <GoogleLogin />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
